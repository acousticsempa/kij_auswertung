clear all

num_el = 4;     %Anzahl elemente

num_shaker = 3; %Anzahl shaker pro element

num_acc = 24;    %Anzahl Aufnehmer

num_acc_shaker = 6;  %Anzahl der Accelerometer pro shaker pro element

num_acc_element = num_acc/num_el; %Anzahl Aufnehmer pro Element

num_shaker_total = num_el*num_shaker;

num_sets = num_acc_shaker/num_acc_element;

num_shaker_BG = 9;

num_BG =1;
m_date = '15.5.2018';
c_type = 'HBV T-junction';
wall_type = 'stud wall';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% choose reverb-time type and average type

RT_type = 1; % for T5 = 1, T10 =2

avg_type = 1; % for mean = 1, for median = 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RT_name = {'T5','T10'};
avg_name ={'mean','median'};

L = [4.17 4.17 4.17 4.17];% junction length
fc = [1600,1600,100,200];% critical frequency in order of the elements
mass = [45,45,276,46];% mass per unit area in order of the elements
area = [12.6,12.3,22.7,22.7];%area of the elements
c0 = 343; %speed of air
fc_ref =1000;


filename_T = 'RT_4_elements_4.3.2014.mat';

Element_name={'Wall 1','Wall 2','Concrete I','Concrete II'};
Difference_spec = {'Wall 1 - Wall 2','Wall 1 - Concrete','Wall 1 - Timber';'Wall 2 - Wall 1','Wall 2 - Concrete','Wall 2 - Timber';'Concrete - Wall 1','Concrete - Wall 2','Concrete - Timber';'Timber - Wall 1','Timber - Wall 2','Timber - Concrete'};
sMess = {'Mean Wall 1 Wall 2','Mean Wall 1 Concrete','Mean Wall 1 Timber','Mean Wall 2 Concrete','Mean Wall 2 Timber','Mean Concrete Timber'};
col = {'b','r','g'};
file_base = 'Betonverbinder_Shakerpos_';
file_end = 'acc_rms_';
set_base = 'Var';

data_folder = 'G:\509-BAUAKUSTIK\0-Aktuelle_Auftr�ge\2018_Forschung\Lignum_HBV_2018\Dvij_HBV_St�nderwand_Betonverbinder'

cd(data_folder);

v0 = 10^-9; %reference velocity

filename = [  file_end file_base num2str(1) '_' num2str(1)  '.mat'];
load(filename);

expr = ['f = ' set_base '10_X; '];  % read the frequency vector
eval(expr);
datlength = length(f);

% Read PAK data

data_velo = zeros(num_shaker_total, num_sets, num_acc, datlength);

for shaker = 1 : num_shaker_total;
    
    for set = 1 : num_sets;
        
        
            filename = [file_end file_base num2str(shaker) '_' num2str(set)  '.mat'];
        
                            
            load(filename);
           
            
        for acc = 1 : num_acc
            
            name_test = [set_base num2str(acc)];
           
                             
               eval(['data_velo(shaker, set, acc, :) = ' name_test ';']);
               
            
            
        end
    end
    
end

data_velo_2 = zeros(num_shaker_total, num_el, num_acc_shaker, datlength);

channel = 1 : num_acc;

channel = reshape(channel, num_acc_element, num_el);
%channel = channel(:,[1 3 4 2]); % Zuweisung der Kan�le zu den Elementen

start_acc = min(channel,[],1);
end_acc = max(channel,[],1);


for shaker = 1 : num_shaker_total;
    
    for set = 1 : num_sets;
    
        for n = 1 : num_el
        
            data_velo_2(shaker, n, (set-1)*num_acc_element+1:set*num_acc_element,:) = data_velo(shaker, set, start_acc(n):end_acc(n),:); 
        
        
        end
        
    end
end


Source_element.Shaker = struct('Response', zeros(num_el, num_acc_shaker, datlength));

shaker_pos = 1 : num_shaker_total;

shaker_pos = reshape(shaker_pos, num_shaker, num_el);

% start_shaker = min(shaker,[],1);
% end_shaker = max(shaker,[],1);

for source = 1 : num_el;
    
    for shaker = 1 : num_shaker
    
        Source_element(source).Shaker(shaker).Response = squeeze(data_velo_2(shaker_pos(shaker, source), :, :, :));
    
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read background data

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data_velo = zeros(num_shaker_total, num_sets, num_acc, datlength);

for shaker = 1 : num_shaker_total;
    
    for set = 1 : num_sets;
        
        
            filename = [file_end file_base num2str(num_shaker_BG) '_' num2str(num_BG) '_BG'  '.mat'];
        
            load(filename);
        
        for acc = 1 : num_acc
            
            name_test = [set_base num2str(acc)];
            
            eval(['data_velo(shaker, set, acc, :) = ' name_test ';']);
    
        end
    end
    
end

data_velo_2 = zeros(num_shaker_total, num_el, num_acc_shaker, datlength);

channel = 1 : num_acc;

channel = reshape(channel, num_acc_element, num_el);
%channel = channel(:,[1 3 4 2]);  


start_acc = min(channel,[],1);
end_acc = max(channel,[],1);


for shaker = 1 : num_shaker_total;
    
    for set = 1 : num_sets;
    
        for n = 1 : num_el
        
            data_velo_2(shaker, n, (set-1)*num_acc_element+1:set*num_acc_element,:) = data_velo(shaker, set, start_acc(n):end_acc(n),:); 
        
        
        end
        
    end
end


Source_element_BG.Shaker = struct('Response', zeros(num_el, num_acc_shaker, datlength));

shaker_pos = 1 : num_shaker_total;

shaker_pos = reshape(shaker_pos, num_shaker, num_el);

% start_shaker = min(shaker,[],1);
% end_shaker = max(shaker,[],1);

for source = 1 : num_el;
    
    for shaker = 1 : num_shaker
    
        Source_element_BG(source).Shaker(shaker).Response = squeeze(data_velo_2(shaker_pos(shaker, source), :, :, :));
    
    end

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Background correction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Source_element_sqr.Shaker = struct('Response', zeros(num_el, num_acc_shaker, datlength));

% square the measurement data

for source = 1:num_el;
   for shaker = 1:num_shaker;
    for resp_el = 1:num_el;
        for acc = 1:num_acc_shaker;
            
          Source_element_sqr(source).Shaker(shaker).Response(resp_el,acc,:)= (Source_element(source).Shaker(shaker).Response(resp_el,acc,:)).^2;  
          Source_element_BG_sqr(source).Shaker(shaker).Response(resp_el,acc,:)= (Source_element_BG(source).Shaker(shaker).Response(resp_el,acc,:)).^2;    
        end
    end
  end
end



for source = 1:num_el;
   for shaker = 1:num_shaker;
    for resp_el = 1:num_el;
        for acc = 1:num_acc_shaker;
            for freq = 1:datlength
          Source_element_corr(source).Shaker(shaker).Response(resp_el,acc,freq)= (Source_element_sqr(source).Shaker(shaker).Response(resp_el,acc,freq))-(Source_element_BG_sqr(source).Shaker(shaker).Response(resp_el,acc,freq));
             
                   if   Source_element_BG_sqr(source).Shaker(shaker).Response(resp_el,acc,freq) >= 0.25*(Source_element_sqr(source).Shaker(shaker).Response(resp_el,acc,freq));
                        
                        Source_element_corr(source).Shaker(shaker).Response(resp_el,acc,freq)= NaN;           
                   end  
            end    
        end
    end
  end
end

% average of accellerometer positions per element


for source = 1:num_el;
   for shaker = 1:num_shaker;
    for resp_el = 1:num_el;
      
        if source == 3;
            if resp_el == 3 && resp_el == 4;  
             Source_element_mean(source).Shaker(shaker).Response(resp_el,:)= nanmean(Source_element_corr(source).Shaker(shaker).Response(resp_el,1:6,:));
            else
            Source_element_mean(source).Shaker(shaker).Response(resp_el,:)= nanmean(Source_element_corr(source).Shaker(shaker).Response(resp_el,:,:));
            end
                
        elseif   source == 4;
            if resp_el == 3 && resp_el == 4; 
             Source_element_mean(source).Shaker(shaker).Response(resp_el,:)= nanmean(Source_element_corr(source).Shaker(shaker).Response(resp_el,7:12,:));
            else
             Source_element_mean(source).Shaker(shaker).Response(resp_el,:)= nanmean(Source_element_corr(source).Shaker(shaker).Response(resp_el,:,:));   
            end
        else 
            
          Source_element_mean(source).Shaker(shaker).Response(resp_el,:)= nanmean(Source_element_corr(source).Shaker(shaker).Response(resp_el,:,:));  
        end  
    end
  end
end
%calculate levels

for source = 1:num_el;
   for shaker = 1:num_shaker;
    for resp_el = 1:num_el;
      
          Source_element_mean_Lev(source).Shaker(shaker).Response(resp_el,:)= 10*log10(Source_element_mean(source).Shaker(shaker).Response(resp_el,:)/v0^2);
     
    end
  end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% difference calculation for each scource element and shaker-position

Source_element_diff.Shaker = struct('Diff', zeros(num_el-1, datlength));

diff_1 = [2,3,4];
diff_2 = [1,3,4];
diff_3 = [1,2,4];
diff_4 = [1,2,3];



for source = 1: num_el;
   for shaker = 1:num_shaker;
     x = 1;
     
     if source == 1;
         Diff_type = diff_1;
     elseif source == 2;
         Diff_type = diff_2;
     elseif source == 3;
         Diff_type = diff_3; 
     else
         Diff_type = diff_4;
      end 
     
        for diff = Diff_type;
    
         Source_element_diff(source).Shaker(shaker).Diff(x,:) = Source_element_mean_Lev(source).Shaker(shaker).Response(source,:)-(Source_element_mean_Lev(source).Shaker(shaker).Response(diff,:)) ;
        x = x +1;
        end
       
      
  end
end

%calculate the  arithmetic average of the shakerpositions per element

Source_diff = zeros(num_el,num_shaker,num_el-1,datlength);

for source = 1:num_el;
    
      for shaker = 1: num_shaker
         for diff_num = 1:num_el-1
           Source_diff(source,shaker,diff_num,:) = Source_element_diff(source).Shaker(shaker).Diff(diff_num,:);
         end
      end
end


Source_shaker_mean = squeeze(nanmean(Source_diff,2));
Source_shaker_SD = squeeze(nanstd(Source_diff,0,2));


Diff_mean_E12 = squeeze(0.5*(Source_shaker_mean(1,1,:)+Source_shaker_mean(2,1,:)));
Diff_mean_E13 = squeeze(0.5*(Source_shaker_mean(1,2,:)+Source_shaker_mean(3,1,:)));
Diff_mean_E14 = squeeze(0.5*(Source_shaker_mean(1,3,:)+Source_shaker_mean(4,1,:)));
Diff_mean_E23 = squeeze(0.5*(Source_shaker_mean(2,2,:)+Source_shaker_mean(3,2,:)));
Diff_mean_E24 = squeeze(0.5*(Source_shaker_mean(2,3,:)+Source_shaker_mean(4,2,:)));
Diff_mean_E34 = squeeze(0.5*(Source_shaker_mean(3,3,:)+Source_shaker_mean(4,3,:)));

% save directional averaged velocity level differences  in struct


Dvij_mean = struct('Dv12',Diff_mean_E12,'Dv13',Diff_mean_E13,'Dv14',Diff_mean_E14,'Dv23',Diff_mean_E23,'Dv24',Diff_mean_E24,'Dv34',Diff_mean_E34,'Freq',f);
save(['Dvij_mean_',m_date,'.mat'],'Dvij_mean');







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plot all differences in one figure

figobj = figure('Name',['Velocity level differences, summary of ' ,num2str(num_el)  ' elements',m_date ]);
x = 1;
for o = 1:num_el


  for n = 1 :num_el-1;
    for m = 1:diff_num; 
      
    subplot(num_el,diff_num,x)
    
    semilogx(f,squeeze(Source_diff(o,m,n,:)));
    hold all
    

     xlim([50 5000]);

     xlabel('Frequency [Hz]');
     ylabel(['Velocity level [dB] re ', num2str(v0)]);
     grid on
     title_name = ['Excitation on ', char(Element_name(1,o)),', ', char(Difference_spec(o,n))];
     
 title(title_name)
    end
    semilogx(f,squeeze(Source_shaker_mean(o,n,:)), 'Color', 'black', 'Linewidth', 2);
    errorbar(f,squeeze(Source_shaker_mean(o,n,:)),squeeze(Source_shaker_SD(o,n,:)),'rx'); 
  x =x+1;
  end
  mastername = [' Velocity levels differences between ',num2str(num_el) , ' elements with ' ,num2str(num_shaker),' shakerpositions, ',m_date,', ',c_type ', ',wall_type];
  annotation('textbox', [0 0.88 1 0.1], ...
     'String',[mastername], ...
     'EdgeColor', 'none', ...
     'HorizontalAlignment', 'center','Fontsize',14);
    
end

%saveas(gcf,'VLD_4_el_3_shaker_pos','fig'); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plot of the direction averaged difference for every path

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figobj2 = figure('Name',['Velocity level differences, summary of ' ,num2str(num_el)  ' elements, ',m_date ', ',wall_type ]);

subplot(3,2,1);
semilogx(f,squeeze(Source_shaker_mean(1,1,:)),'DisplayName',Difference_spec(1,1));
hold on
semilogx(f,squeeze(Source_shaker_mean(2,1,:)),'r','DisplayName',Difference_spec(2,1));
hold on
semilogx(f,Diff_mean_E12,'Color','black','Linewidth', 2,'DisplayName','Mean');
xlim([50 5000]);
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);
legend('Location','NW');
grid on
subplot(3,2,2);
semilogx(f,squeeze(Source_shaker_mean(1,2,:)),'DisplayName',Difference_spec(1,2));
hold on
semilogx(f,squeeze(Source_shaker_mean(3,1,:)),'r','DisplayName',Difference_spec(3,1));
hold on
semilogx(f,Diff_mean_E13,'Color','black','Linewidth', 2,'DisplayName','Mean');
xlim([50 5000]);
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);
legend('Location','NW');
grid on
subplot(3,2,3);
semilogx(f,squeeze(Source_shaker_mean(1,3,:)),'DisplayName',Difference_spec(1,3));
hold on
semilogx(f,squeeze(Source_shaker_mean(3,1,:)),'r','DisplayName',Difference_spec(4,1));
hold on
semilogx(f,Diff_mean_E14,'Color','black','Linewidth', 2,'DisplayName','Mean');
xlim([50 5000]);
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);
legend('Location','NW');
grid on
subplot(3,2,4);
semilogx(f,squeeze(Source_shaker_mean(2,2,:)),'DisplayName',Difference_spec(2,2));
hold on
semilogx(f,squeeze(Source_shaker_mean(3,2,:)),'r','DisplayName',Difference_spec(3,2));
hold on
semilogx(f,Diff_mean_E23,'Color','black','Linewidth', 2,'DisplayName','Mean');
xlim([50 5000]);
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);
legend('Location','NW');
grid on
subplot(3,2,5);
semilogx(f,squeeze(Source_shaker_mean(2,3,:)),'DisplayName',Difference_spec(2,3));
hold on
semilogx(f,squeeze(Source_shaker_mean(4,2,:)),'r','DisplayName',Difference_spec(4,2));
hold on
semilogx(f,Diff_mean_E24,'Color','black','Linewidth', 2,'DisplayName','Mean');
xlim([50 5000]);
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);
legend('Location','NW');
grid on
subplot(3,2,6);
semilogx(f,squeeze(Source_shaker_mean(3,3,:)),'DisplayName',Difference_spec(3,3));
hold on
semilogx(f,squeeze(Source_shaker_mean(4,3,:)),'r','DisplayName',Difference_spec(4,3));
hold on
semilogx(f,Diff_mean_E34,'Color','black','Linewidth', 2,'DisplayName','Mean');
xlim([50 5000]);
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);
legend('Location','NW');
grid on
mastername = [' Velocity levels differences, mean of ' ,num2str(num_shaker),' shakerpositions per element, ',m_date,', ',c_type ', ',wall_type];
  annotation('textbox', [0 0.88 1 0.1], ...
     'String',[mastername], ...
     'EdgeColor', 'none', ...
     'HorizontalAlignment', 'center','Fontsize',14);


%saveas(gcf,'VLD_shaker_mean','fig'); 

 figobj3 = figure('Name','Mean-Differences');



semilogx(f,Diff_mean_E12,'b','LineWidth',1.5,'DisplayName',sMess(1,1));
hold on
semilogx(f,Diff_mean_E13,'r','LineWidth',1.5,'DisplayName',sMess(1,2));
hold on
semilogx(f,Diff_mean_E14,'g','LineWidth',1.5,'DisplayName',sMess(1,3));
hold on
semilogx(f,Diff_mean_E23,'m','LineWidth',1.5,'DisplayName',sMess(1,4));
hold on
semilogx(f,Diff_mean_E24,'c','LineWidth',1.5,'DisplayName',sMess(1,5));
hold on
semilogx(f,Diff_mean_E34,'k','LineWidth',1.5,'DisplayName',sMess(1,6));
xlim([40 5000]);

%set(gca,'XTick',f);
%set(gca, 'XTickLabel',f(:,1));
xlabel('Frequency [Hz]');
ylabel(['Velocity level [dB] re ', num2str(v0)]);

legend('Location','EastOutside');
grid on
title(['Direction averaged velocity level differences, ',m_date ', ',c_type ', ',wall_type ],'FontSize',14 );

%saveas(gcf,'VLD_mean','fig'); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculation of Kij in single direction



load(filename_T);% load reverb time data

datlength2 = length(RT(1).Freq_T(:,1));

f_T_min = min(RT(1).Freq_T(:,1));
f_max = max(f);

for n = 1:datlength
if f(n,1) == f_T_min;   % find the start in PAK frequenzy vector
    
    f_start = n;
    
end
end

for n = 1:datlength2
if RT(1).Freq_T(n,1) == f_max;   % find the stop in Reverb time  vector
    
    f_stop = n;
    
end
end

f_all(:,1) = RT(1).Freq_T(1:f_stop,1);




for n = 1:length(f_all);
    
    if avg_type == 1;

K_E12(n,1) = Source_shaker_mean(1,1,n+(f_start-1))+10*log10((mass(1,1)*sqrt(fc(1,2))/(mass(1,2)*sqrt(fc(1,1)))))+10*log10((c0*L(1,1)*RT(2).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));
K_E21(n,1) = Source_shaker_mean(2,1,n+(f_start-1))+10*log10((mass(1,2)*sqrt(fc(1,1))/(mass(1,1)*sqrt(fc(1,2)))))+10*log10((c0*L(1,1)*RT(1).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));

K_E13(n,1) = Source_shaker_mean(1,3,n+(f_start-1))+10*log10((mass(1,1)*sqrt(fc(1,3))/(mass(1,3)*sqrt(fc(1,1)))))+10*log10((c0*L(1,1)*RT(3).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,3))));
K_E31(n,1) = Source_shaker_mean(3,1,n+(f_start-1))+10*log10((mass(1,3)*sqrt(fc(1,1))/(mass(1,1)*sqrt(fc(1,3)))))+10*log10((c0*L(1,1)*RT(1).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));

K_E14(n,1) = Source_shaker_mean(1,1,n+(f_start-1))+10*log10((mass(1,1)*sqrt(fc(1,4))/(mass(1,4)*sqrt(fc(1,1)))))+10*log10((c0*L(1,1)*RT(4).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,4))));
K_E41(n,1) = Source_shaker_mean(4,1,n+(f_start-1))+10*log10((mass(1,4)*sqrt(fc(1,1))/(mass(1,1)*sqrt(fc(1,4)))))+10*log10((c0*L(1,1)*RT(1).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));

K_E23(n,1) = Source_shaker_mean(2,2,n+(f_start-1))+10*log10((mass(1,2)*sqrt(fc(1,3))/(mass(1,3)*sqrt(fc(1,2)))))+10*log10((c0*L(1,1)*RT(3).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,3))));
K_E32(n,1) = Source_shaker_mean(3,2,n+(f_start-1))+10*log10((mass(1,3)*sqrt(fc(1,2))/(mass(1,2)*sqrt(fc(1,3)))))+10*log10((c0*L(1,1)*RT(2).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));

K_E24(n,1) = Source_shaker_mean(2,3,n+(f_start-1))+10*log10((mass(1,2)*sqrt(fc(1,4))/(mass(1,4)*sqrt(fc(1,2)))))+10*log10((c0*L(1,1)*RT(4).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,4))));
K_E42(n,1) = Source_shaker_mean(4,2,n+(f_start-1))+10*log10((mass(1,4)*sqrt(fc(1,2))/(mass(1,2)*sqrt(fc(1,4)))))+10*log10((c0*L(1,1)*RT(2).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));

K_E34(n,1) = Source_shaker_mean(3,3,n+(f_start-1))+10*log10((mass(1,3)*sqrt(fc(1,4))/(mass(1,4)*sqrt(fc(1,3)))))+10*log10((c0*L(1,1)*RT(4).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,4))));
K_E43(n,1) = Source_shaker_mean(4,3,n+(f_start-1))+10*log10((mass(1,4)*sqrt(fc(1,3))/(mass(1,3)*sqrt(fc(1,4)))))+10*log10((c0*L(1,1)*RT(3).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,3))));


    else
K_E12(n,1) = Source_shaker_mean(1,1,n+(f_start-1))+10*log10((mass(1,1)*sqrt(fc(1,2))/(mass(1,2)*sqrt(fc(1,1)))))+10*log10((c0*L(1,1)*RT(2).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));
K_E21(n,1) = Source_shaker_mean(2,1,n+(f_start-1))+10*log10((mass(1,2)*sqrt(fc(1,1))/(mass(1,1)*sqrt(fc(1,2)))))+10*log10((c0*L(1,1)*RT(1).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));

K_E13(n,1) = Source_shaker_mean(1,3,n+(f_start-1))+10*log10((mass(1,1)*sqrt(fc(1,3))/(mass(1,3)*sqrt(fc(1,1)))))+10*log10((c0*L(1,1)*RT(3).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,3))));
K_E31(n,1) = Source_shaker_mean(3,1,n+(f_start-1))+10*log10((mass(1,3)*sqrt(fc(1,1))/(mass(1,1)*sqrt(fc(1,3)))))+10*log10((c0*L(1,1)*RT(1).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));

K_E14(n,1) = Source_shaker_mean(1,1,n+(f_start-1))+10*log10((mass(1,1)*sqrt(fc(1,4))/(mass(1,4)*sqrt(fc(1,1)))))+10*log10((c0*L(1,1)*RT(4).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,4))));
K_E41(n,1) = Source_shaker_mean(4,1,n+(f_start-1))+10*log10((mass(1,4)*sqrt(fc(1,1))/(mass(1,1)*sqrt(fc(1,4)))))+10*log10((c0*L(1,1)*RT(1).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));

K_E23(n,1) = Source_shaker_mean(2,2,n+(f_start-1))+10*log10((mass(1,2)*sqrt(fc(1,3))/(mass(1,3)*sqrt(fc(1,2)))))+10*log10((c0*L(1,1)*RT(3).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,3))));
K_E32(n,1) = Source_shaker_mean(3,2,n+(f_start-1))+10*log10((mass(1,3)*sqrt(fc(1,2))/(mass(1,2)*sqrt(fc(1,3)))))+10*log10((c0*L(1,1)*RT(2).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));

K_E24(n,1) = Source_shaker_mean(2,3,n+(f_start-1))+10*log10((mass(1,2)*sqrt(fc(1,4))/(mass(1,4)*sqrt(fc(1,2)))))+10*log10((c0*L(1,1)*RT(4).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,4))));
K_E42(n,1) = Source_shaker_mean(4,2,n+(f_start-1))+10*log10((mass(1,4)*sqrt(fc(1,2))/(mass(1,2)*sqrt(fc(1,4)))))+10*log10((c0*L(1,1)*RT(2).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));

K_E34(n,1) = Source_shaker_mean(3,3,n+(f_start-1))+10*log10((mass(1,3)*sqrt(fc(1,4))/(mass(1,4)*sqrt(fc(1,3)))))+10*log10((c0*L(1,1)*RT(4).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,4))));
K_E43(n,1) = Source_shaker_mean(4,3,n+(f_start-1))+10*log10((mass(1,4)*sqrt(fc(1,3))/(mass(1,3)*sqrt(fc(1,4)))))+10*log10((c0*L(1,1)*RT(3).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,3))));

    end        
        
        

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculation of Kij acording 10848-1 equation 13 from mean Dvij

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for n = 1:length(f_all)
    if avg_type == 1;
     a1(n,1) = (2.2*pi^2*area(1,1))/(RT(1).mean(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     a2(n,1) = (2.2*pi^2*area(1,2))/(RT(2).mean(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     a3(n,1) = (2.2*pi^2*area(1,3))/(RT(3).mean(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     a4(n,1) = (2.2*pi^2*area(1,4))/(RT(4).mean(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     
    else
     a1(n,1) = (2.2*pi^2*area(1,1))/(RT(1).median(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     a2(n,1) = (2.2*pi^2*area(1,2))/(RT(2).median(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     a3(n,1) = (2.2*pi^2*area(1,3))/(RT(3).median(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
     a4(n,1) = (2.2*pi^2*area(1,4))/(RT(4).median(n,RT_type)*c0*sqrt(f(n+f_start-1,1)/fc_ref));
        
        
    end
end
datlength3 = length(a1);
a = zeros(datlength3,num_el);

a(:,1)= a1;
a(:,2)= a2;
a(:,3)= a3;
a(:,4)= a4;

for n = 1:length(f_all)
    Kij_12a(n,1) = Diff_mean_E12(n+f_start-1)+10*log10(L(1,1)./sqrt(a1(n,1)*a2(n,1)));%10848-1 equation 13
    Kij_13a(n,1) = Diff_mean_E13(n+f_start-1)+10*log10(L(1,1)./sqrt(a1(n,1)*a3(n,1)));%10848-1 equation 13
    Kij_14a(n,1) = Diff_mean_E14(n+f_start-1)+10*log10(L(1,1)./sqrt(a1(n,1)*a4(n,1)));%10848-1 equation 13
    Kij_23a(n,1) = Diff_mean_E23(n+f_start-1)+10*log10(L(1,1)./sqrt(a2(n,1)*a3(n,1)));%10848-1 equation 13
    Kij_24a(n,1) = Diff_mean_E24(n+f_start-1)+10*log10(L(1,1)./sqrt(a2(n,1)*a4(n,1)));%10848-1 equation 13
    Kij_34a(n,1) = Diff_mean_E34(n+f_start-1)+10*log10(L(1,1)./sqrt(a3(n,1)*a4(n,1)));%10848-1 equation 13
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figobj4 = figure('Name','Kij');

subplot(3,2,1);
semilogx(f_all,K_E12,'LineWidth',1.5,'DisplayName',Difference_spec(1,1));
hold on
semilogx(f_all,K_E21,'r','LineWidth',1.5,'DisplayName',Difference_spec(2,1));
hold on
semilogx(f_all,Kij_12a,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','NW');
grid on

subplot(3,2,2);
semilogx(f_all,K_E13,'LineWidth',1.5,'DisplayName',Difference_spec(1,2));
hold on
semilogx(f_all,K_E31,'r','LineWidth',1.5,'DisplayName',Difference_spec(3,1));
hold on
semilogx(f_all,Kij_13a,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','SW');
grid on

subplot(3,2,3);
semilogx(f_all,K_E14,'LineWidth',1.5,'DisplayName',Difference_spec(1,3));
hold on
semilogx(f_all,K_E41,'r','LineWidth',1.5,'DisplayName',Difference_spec(4,1));
hold on
semilogx(f_all,Kij_14a,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','NW');
grid on

subplot(3,2,4);
semilogx(f_all,K_E23,'LineWidth',1.5,'DisplayName',Difference_spec(2,2));
hold on
semilogx(f_all,K_E32,'r','LineWidth',1.5,'DisplayName',Difference_spec(3,2));
hold on
semilogx(f_all,Kij_23a,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','NW');
grid on

subplot(3,2,5);
semilogx(f_all,K_E24,'LineWidth',1.5,'DisplayName',Difference_spec(2,3));
hold on
semilogx(f_all,K_E42,'r','LineWidth',1.5,'DisplayName',Difference_spec(4,2));
hold on
semilogx(f_all,Kij_24a,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','NW');
grid on

subplot(3,2,6);
semilogx(f_all,K_E34,'LineWidth',1.5,'DisplayName',Difference_spec(3,3));
hold on
semilogx(f_all,K_E43,'r','LineWidth',1.5,'DisplayName',Difference_spec(4,3));
hold on
semilogx(f_all,Kij_34a,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','SW');
grid on

mastername = (['Single direction K_{ij} for junction with 4 elements, masses: CLT1 CLT2: ', num2str(mass(1,1)), 'kg/m^{2}, Concrete: ' , num2str(mass(1,3)), 'kg/m^{2}, OSB: ', num2str(mass(1,4)), 'kg/m^{2}, RT:  ' char(RT_name(1,RT_type)) ', Avg:  ' char(avg_name(1,avg_type)),', ',m_date,', ',c_type]);
  annotation('textbox', [0 0.88 1 0.1], ...
     'String',[mastername], ...
     'EdgeColor', 'none', ...
     'HorizontalAlignment', 'center','Fontsize',14);


% saveas(gcf,['Single_direction_ Kij_',char(RT_name(1,RT_type)),'_',char(avg_name(1,avg_type)),'_',num2str(mass(1,3)),'_',num2str(mass(1,4))],'fig'); 



Kij_12 = Diff_mean_E12+10*log10(L(1,1)/sqrt(area(1,1)*area(1,2))); %10848-1 equation 14

for n = 1:length(f_all);
    
   Kij_12c(n,1) = Source_shaker_mean(1,1,n+(f_start-1))+10*log10((mass(1,1)*c0*L(1,1)*RT(2).mean(n,RT_type))/(mass(1,2)*sqrt(fc(1,1)*f_all(n,1))*2.2*(pi^2)*area(1,2)))+5*log10(fc(1,2)/fc_ref);
   Kij_21c(n,1) = Source_shaker_mean(2,1,n+(f_start-1))+10*log10((mass(1,2)*c0*L(1,1)*RT(1).mean(n,RT_type))/(mass(1,1)*sqrt(fc(1,2)*f_all(n,1))*2.2*(pi^2)*area(1,1)))+5*log10(fc(1,1)/fc_ref);
end


figobj5 = figure('Name','Kij 2');


semilogx(f_all,K_E12,'LineWidth',1.5,'DisplayName',Difference_spec(1,1));
hold on
semilogx(f_all,K_E21,'r','LineWidth',1.5,'DisplayName',Difference_spec(2,1));
hold on
semilogx(f_all,Kij_12(f_start:end,1),'c','LineWidth',1.5,'DisplayName','10848-1 eq. 14');
hold on
semilogx(f_all,Kij_12a(:,1),'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');

xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('K_{ij} [dB] ');
legend('Location','SW');
grid on
title(['K_{ij} , 10848 vs. single path calculation ',char(RT_name(1,RT_type)),', ',char(avg_name(1,avg_type)),', ',m_date,', ',num2str(mass(1,3)),'kg, ',num2str(mass(1,4)),'kg'  ],'FontSize',14 );


for n = 1:length(f_all);
if avg_type == 1;
  Var1(n,1) = 10*log10((c0*L(1,1)*RT(2).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));
  Var2(n,1) = 10*log10((c0*L(1,1)*RT(1).mean(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));
else
  Var1(n,1) = 10*log10((c0*L(1,1)*RT(2).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,2))));
  Var2(n,1) = 10*log10((c0*L(1,1)*RT(1).median(n,RT_type)*sqrt(f(n+(f_start-1),1))/(sqrt(fc_ref)*2.2*pi^2*area(1,1))));  
    
end

Var3(n,1) = 10*log10(L(1,1)/(a1(n,1)*a2(n,1)));
Var4(n,1) = 10*log10(L(1,1)/(area(1,1)*area(1,2)));
end


figobj6 = figure('Name','Correction for Dv,ij');


semilogx(f_all,Var1,'LineWidth',1.5,'DisplayName',Difference_spec(1,1));
hold on
semilogx(f_all,Var2,'r','LineWidth',1.5,'DisplayName',Difference_spec(2,1));
hold on
semilogx(f_all,Var3,'g','LineWidth',1.5,'DisplayName','10848-1 eq. 13');
hold on
semilogx(f_all,Var4,'c','LineWidth',1.5,'DisplayName','10848-1 eq. 14');


xlim([40 5000]);
xlabel('Frequency [Hz]');
ylabel('Correction values  [dB] ');
legend('Location','SW');
grid on
title(['Correction values for D_{v,ij}  10848 vs. single path calculation ',char(RT_name(1,RT_type)),', ',char(avg_name(1,avg_type)),', ',m_date,', ',num2str(mass(1,3)),'kg, ',num2str(mass(1,4)),'kg' ],'FontSize',14 );

saveas(gcf,['Corr_values_Dvij_',char(RT_name(1,RT_type)),'_',char(avg_name(1,avg_type)),'_',num2str(mass(1,3)),'_',num2str(mass(1,4))],'fig'); 

%save single path Kij in struct

if RT_type == 1 && avg_type == 1;
Kij_T5_mean = struct('Kij12',K_E12,'Kij13',K_E13,'Kij14',K_E14,'Kij21',K_E21,'Kij23',K_E23,'Kij24',K_E24,'Kij31',K_E31,'Kij32',K_E32,'Kij34',K_E34,'Kij41',K_E41,'Kij42',K_E42,'Kij43',K_E43,'Freq',f_all);
save(['KijT5mean_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kij_T5_mean');
elseif RT_type == 2 && avg_type == 1;
    
Kij_T10_mean = struct('Kij12',K_E12,'Kij13',K_E13,'Kij14',K_E14,'Kij21',K_E21,'Kij23',K_E23,'Kij24',K_E24,'Kij31',K_E31,'Kij32',K_E32,'Kij34',K_E34,'Kij41',K_E41,'Kij42',K_E42,'Kij43',K_E43,'Freq',f_all);   
save(['KijT10mean_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kij_T10_mean');    
elseif RT_type == 1 && avg_type == 2;
    
Kij_T5_median = struct('Kij12',K_E12,'Kij13',K_E13,'Kij14',K_E14,'Kij21',K_E21,'Kij23',K_E23,'Kij24',K_E24,'Kij31',K_E31,'Kij32',K_E32,'Kij34',K_E34,'Kij41',K_E41,'Kij42',K_E42,'Kij43',K_E43,'Freq',f_all);
save(['KijT5median_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kij_T5_median');
else

Kij_T10_median = struct('Kij12',K_E12,'Kij13',K_E13,'Kij14',K_E14,'Kij21',K_E21,'Kij23',K_E23,'Kij24',K_E24,'Kij31',K_E31,'Kij32',K_E32,'Kij34',K_E34,'Kij41',K_E41,'Kij42',K_E42,'Kij43',K_E43,'Freq',f_all);
save(['KijT10median_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kij_T10_median');
end

% save Kij calculated acording 10848-1 in struct

if RT_type == 1 && avg_type == 1;
Kijmean_T5_mean = struct('K12',Kij_12a,'K13',Kij_13a,'K14',Kij_14a,'K23',Kij_23a,'K24',Kij_24a,'K34',Kij_34a,'Freq',f_all);
save(['Kij10848T5mean_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kijmean_T5_mean');
elseif RT_type == 2 && avg_type == 1;

Kijmean_T10_mean = struct('K12',Kij_12a,'K13',Kij_13a,'K14',Kij_14a,'K23',Kij_23a,'K24',Kij_24a,'K34',Kij_34a,'Freq',f_all);
save(['Kij10848T10mean_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kijmean_T10_mean');    
elseif RT_type == 1 && avg_type == 2;
    
Kijmean_T5_median = struct('K12',Kij_12a,'K13',Kij_13a,'K14',Kij_14a,'K23',Kij_23a,'K24',Kij_24a,'K34',Kij_34a,'Freq',f_all);
save(['Kij10848T5median_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kijmean_T5_median');
else

Kijmean_T10_median = struct('K12',Kij_12a,'K13',Kij_13a,'K14',Kij_14a,'K23',Kij_23a,'K24',Kij_24a,'K34',Kij_34a,'Freq',f_all);
save(['Kij10848T10median_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Kijmean_T10_median');
end



Aeq_L_T5mean = struct('a',zeros(datlength3,num_el),'Freq',f_all,'ReadMe',['order of columns = order of elements']);
Aeq_L_T10mean = struct('a',zeros(datlength3,num_el),'Freq',f_all,'ReadMe',['order of columns = order of elements']);
Aeq_L_T5median = struct('a',zeros(datlength3,num_el),'Freq',f_all,'ReadMe',['order of columns = order of elements']);
Aeq_L_T10median = struct('a',zeros(datlength3,num_el),'Freq',f_all,'ReadMe',['order of columns = order of elements']);


for n = 1 : datlength3
if RT_type == 1 && avg_type == 1;
 Aeq_L_T5mean.a(n,:) = a(n,:);   
 save(['Aeq_L_T5mean_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Aeq_L_T5mean');
 
elseif RT_type == 2 && avg_type == 1;
 Aeq_L_T10mean.a(n,:) = a(n,:);    
 save(['Aeq_L_T10mean_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Aeq_L_T10mean'); 
 
elseif RT_type == 1 && avg_type == 2;
Aeq_L_T5median.a(n,:) = a(n,:);     
save(['Aeq_L_T5median_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Aeq_L_T5median'); 

else   
Aeq_L_T10median.a(n,:) = a(n,:);  
save(['Aeq_L_T10median_',m_date,'_',num2str(mass(1,3)),'_',num2str(mass(1,4)),'.mat'],'Aeq_L_T5median');

end    
end    


